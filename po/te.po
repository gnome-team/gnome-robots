# translation of te.po to Telugu
# Telugu translation of gnome-games.
# Copyright (C) 2011, 2012 Swecha Telugu Localisation Team <localisation@swecha.net>
# This file is distributed under the same license as the gnome-games package.
#
# Bharat Kumar Jonnalagadda <bharath@swecha.net>, 2007.
# Krishna Babu K <kkrothap@redhat.com>, 2009.
# veera reddy v <veerared.e@gmail.com>,2011.
# Krishnababu Krothapalli <kkrothap@redhat.com>, 2011.
# sasi <sasi@swecha.net>, 2012.
# Bhuvan Krishna <bhuvan@swecha.net>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: te\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-04 00:09+0100\n"
"PO-Revision-Date: 2012-03-25 13:25+0530\n"
"Last-Translator: Bhuvan Krishna <bhuvan@swecha.net>\n"
"Language-Team: Telugu <indlinux-telugu@lists.sourceforge.net>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"X-Generator: Lokalize 1.2\n"

#: ../data/app-menu.ui.h:1 ../src/gnome-robots.c:256
msgid "_New Game"
msgstr "కొత్త ఆట (_N)"

#: ../data/app-menu.ui.h:2
msgid "_Preferences"
msgstr "అభీష్టాలు"

#: ../data/app-menu.ui.h:3
msgid "_Scores"
msgstr "స్కోర్లు (_S)"

#: ../data/app-menu.ui.h:4
msgid "_Help"
msgstr "సహాయం (_H)"

#: ../data/app-menu.ui.h:5
msgid "_About"
msgstr "గురించి (_A)"

#: ../data/app-menu.ui.h:6
msgid "_Quit"
msgstr "త్యజించు"

#: ../data/gnome-robots.appdata.xml.in.h:1
#, fuzzy
msgid "GNOME Robots"
msgstr "రోబోట్సు"

#: ../data/gnome-robots.appdata.xml.in.h:2 ../data/gnome-robots.desktop.in.h:2
msgid "Avoid the robots and make them crash into each other"
msgstr "రోబోట్సును వేరుచేయండి మరియు అవి వొకదానికి వొకటి గుద్దుకొనునట్లు చేయండి"

#: ../data/gnome-robots.appdata.xml.in.h:3
msgid ""
"It is the distant future – the year 2000. Evil robots are trying to kill "
"you. Avoid the robots or face certain death."
msgstr ""

#: ../data/gnome-robots.appdata.xml.in.h:4
msgid ""
"Fortunately, the robots are extremely stupid and will always move directly "
"towards you. Trick them into colliding into each other, resulting in their "
"destruction, or into the junk piles that result. You can defend yourself by "
"moving the junk piles, or escape to safety with your handy teleportation "
"device."
msgstr ""

#: ../data/gnome-robots.appdata.xml.in.h:5
msgid ""
"Your supply of safe teleports is limited, and once you run out, "
"teleportation could land you right next to a robot, who will kill you. "
"Survive for as long as possible!"
msgstr ""

#: ../data/gnome-robots.appdata.xml.in.h:6
msgid "The GNOME Project"
msgstr ""

#: ../data/gnome-robots.desktop.in.h:1 ../src/gnome-robots.c:223
#: ../src/gnome-robots.c:315 ../src/gnome-robots.c:348
msgid "Robots"
msgstr "రోబోట్సు"

#: ../data/gnome-robots.desktop.in.h:3
msgid "game;arcade;teleport;"
msgstr ""

#: ../data/org.gnome.robots.gschema.xml.h:1
msgid "Show toolbar"
msgstr "పనిముట్ల పట్టాని చూపుము"

#: ../data/org.gnome.robots.gschema.xml.h:2
msgid "Show toolbar. A standard option for toolbars."
msgstr "సాధనపట్టీను చూపుము. సాధనపట్టీల కొరకు వొక ప్రామాణిక ఐచ్చికము."

#: ../data/org.gnome.robots.gschema.xml.h:3
msgid "Robot image theme"
msgstr "రోబోట్ ప్రతిబింబము థీమ్"

#: ../data/org.gnome.robots.gschema.xml.h:4
msgid "Robot image theme. The theme of the images to use for the robots."
msgstr "రోబోట్ ప్రతిబింబము థీమ్. రోబోట్ల కొరకు వుపయోగించుటకు ప్రతిబింబముల యొక్క థీమ్."

#: ../data/org.gnome.robots.gschema.xml.h:5
msgid "Background color"
msgstr "పూర్వరంగం వర్ణము"

#: ../data/org.gnome.robots.gschema.xml.h:6
msgid "Background color. The hex specification of the background color."
msgstr "బ్యాక్‌గ్రౌండ్ రంగు. బ్యాక్‌గ్రౌండ్ రంగు యొక్క హెక్స్ విశదీకరణ."

#: ../data/org.gnome.robots.gschema.xml.h:7
msgid "Game type"
msgstr "ఆట రకం"

#: ../data/org.gnome.robots.gschema.xml.h:8
msgid "Game type. The name of the game variation to use."
msgstr "ఆట రకము. ఉపయోగించుటకు ఆట తేడా నామము."

#: ../data/org.gnome.robots.gschema.xml.h:9
msgid "Use safe moves"
msgstr "భద్రమైన ఎత్తులను ఉపయోగించుము"

#: ../data/org.gnome.robots.gschema.xml.h:10
msgid ""
"Use safe moves. The safe moves option will help you to avoid being killed "
"due to a mistake. If you try to make a move that would lead to your death "
"when there is a safe move available you will not be allowed to proceed."
msgstr ""
"సురక్షిత కదలికలను వుపయోగించుము. తప్పువలన చనిపోకుండా వుండుటకు సురక్షిత కదలికల ఐచ్చికం "
"సహాయబడుతుంది. సురక్షిత కదలిక అందుబాటులో వుంటే మీరు చనిపోవుటకు కారణమయ్యే కదలికను చేయుటకు "
"ప్రయత్నిస్తుంటే మీరు అనుమతించబడరు."

#: ../data/org.gnome.robots.gschema.xml.h:11
msgid "Use super safe moves"
msgstr "సూపర్ రక్షిత కదలికలను వుపయోగించుము"

#: ../data/org.gnome.robots.gschema.xml.h:12
msgid ""
"Use super safe moves. The player is alerted when there is no safe move and "
"the only option is to teleport out."
msgstr ""
"సూపర్ రక్షిత కదలికలను వుపయోగించుము. సురక్షిత కదలిక లేనప్పుడు ఆటగాటు జాగ్రత్త చేయబడతారు మరియు "
"టెలీపోర్టు అవుట్ మాత్రమే ఐచ్చికము."

#: ../data/org.gnome.robots.gschema.xml.h:13
msgid "Enable game sounds"
msgstr "ఆట శబ్దములను చేతనం చేయండి"

#: ../data/org.gnome.robots.gschema.xml.h:14
msgid "Enable game sounds. Play sounds for various events throughout the game."
msgstr "ఆట శబ్దములను చేతనం చేయండి. ఆట మొత్తం వివిధ ఘటనల కొరకు శబ్దములను మ్రోగించండి."

#: ../data/org.gnome.robots.gschema.xml.h:15 ../src/properties.c:503
msgid "Key to move NW"
msgstr "వాయువ్యం వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:16
msgid "The key used to move north-west."
msgstr "వాయువ్య దిశగా జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:17 ../src/properties.c:504
msgid "Key to move N"
msgstr "ఉత్తరం వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:18
msgid "The key used to move north."
msgstr "ఉత్తర వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:19 ../src/properties.c:505
msgid "Key to move NE"
msgstr "ఈశాన్యం వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:20
msgid "The key used to move north-east."
msgstr "ఈశాన్యం వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:21 ../src/properties.c:506
msgid "Key to move W"
msgstr "పడమర వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:22
msgid "The key used to move west."
msgstr "పశ్చిమ వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:23 ../src/properties.c:507
msgid "Key to hold"
msgstr "పట్టివుంచుటకు కీ"

#: ../data/org.gnome.robots.gschema.xml.h:24
msgid "The key used to hold still."
msgstr "ఉపయోగించిన కీ ఇంకా పట్టుకోండి"

#: ../data/org.gnome.robots.gschema.xml.h:25 ../src/properties.c:508
msgid "Key to move E"
msgstr "తూర్పు వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:26
msgid "The key used to move east."
msgstr "తూర్పు వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:27 ../src/properties.c:509
msgid "Key to move SW"
msgstr "నైరుతి వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:28
msgid "The key used to move south-west."
msgstr "నైఋతి వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:29 ../src/properties.c:510
msgid "Key to move S"
msgstr "దక్షిణం వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:30
msgid "The key used to move south."
msgstr "దక్షిణం వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:31 ../src/properties.c:511
msgid "Key to move SE"
msgstr "ఆగ్నేయం వైపుకు జరుగుటకు నొక్కవలసిన మీట"

#: ../data/org.gnome.robots.gschema.xml.h:32
msgid "The key used to move south-east."
msgstr "ఆగ్నేయం వైపుకు జరుగుటకు మీటను నొక్కుము"

#: ../data/org.gnome.robots.gschema.xml.h:33
#, fuzzy
msgid "Width of the window in pixels"
msgstr "ముఖ్య విండోయొక్క వెడల్పు పిగ్జెల్సులో."

#: ../data/org.gnome.robots.gschema.xml.h:34
#, fuzzy
msgid "Height of the window in pixels"
msgstr "ముఖ్య విండోయొక్క ఎత్తు పిగ్జెల్సులో."

#: ../data/org.gnome.robots.gschema.xml.h:35
msgid "true if the window is maximized"
msgstr ""

#: ../src/game.c:343 ../src/game.c:359
msgid ""
"Congratulations, You Have Defeated the Robots!! \n"
"But Can You do it Again?"
msgstr ""
"అభినందనలు, మీరు రోబోట్సును వోడించారు!! \n"
"అయితే మీరు దానిని మరలా చేయగలరా?"

#. This should never happen.
#: ../src/game.c:1110
msgid "There are no teleport locations left!!"
msgstr "అక్కడ ఏ టెలీపోర్టు స్థానములు మిగలలేదు!!"

#: ../src/game.c:1138
msgid "There are no safe locations to teleport to!!"
msgstr "టెలీపోర్టుకు కూడా అక్కడ రక్షిత స్థానములు లేవు!!"

#: ../src/games-controls.c:286
msgid "Unknown Command"
msgstr "అపరిచిత వ్యాఖ్యా"

#: ../src/gnome-robots.c:106
msgid "Classic robots"
msgstr "సాంప్రదాయ రోబోట్సు"

#: ../src/gnome-robots.c:107
msgid "Classic robots with safe moves"
msgstr "రక్షిత కదలికలతో సాంప్రదాయ రోబోట్సు"

#: ../src/gnome-robots.c:108
msgid "Classic robots with super-safe moves"
msgstr "సూపర్-సేఫ్ కదలికలతో సాంప్రదాయ రోబోట్సు"

#: ../src/gnome-robots.c:109
msgid "Nightmare"
msgstr "నైట్‌మేర్"

#: ../src/gnome-robots.c:110
msgid "Nightmare with safe moves"
msgstr "రక్షిత కదలికలతో నైట్‌మేర్"

#: ../src/gnome-robots.c:111
msgid "Nightmare with super-safe moves"
msgstr "సూపర్-సేఫ్ కదలికలతో నైట్‌మేర్"

#: ../src/gnome-robots.c:112
msgid "Robots2"
msgstr "రోబోట్సు2"

#: ../src/gnome-robots.c:113
msgid "Robots2 with safe moves"
msgstr "రక్షిత కదలికలతో రోబోట్సు2"

#: ../src/gnome-robots.c:114
msgid "Robots2 with super-safe moves"
msgstr "సూపర్-సేఫ్ కదలికలతో రోబోట్సు2"

#: ../src/gnome-robots.c:115
msgid "Robots2 easy"
msgstr "రోబోట్సు2 సులవైన"

#: ../src/gnome-robots.c:116
msgid "Robots2 easy with safe moves"
msgstr "రక్షిత కదలికలతో రోబోట్సు2 సులువైన"

#: ../src/gnome-robots.c:117
msgid "Robots2 easy with super-safe moves"
msgstr "సూపర్-సేఫ్ కదలికలతో రోబోట్సు2 సులువైన"

#: ../src/gnome-robots.c:118
msgid "Robots with safe teleport"
msgstr "రక్షిత టెలీపోర్టుతో రోబోట్సు"

#: ../src/gnome-robots.c:119
msgid "Robots with safe teleport with safe moves"
msgstr "రక్షిత కదలికలతో రక్షిత టెలీపోర్టుతో రోబోట్సు"

#: ../src/gnome-robots.c:120
msgid "Robots with safe teleport with super-safe moves"
msgstr "సూపర్-సేఫ్ కదలికలతో రక్షిత టెలీపోర్టుతో రోబోట్సు"

#. Window subtitle. The first %d is the level, the second is the score. \t creates a tab.
#: ../src/gnome-robots.c:156
#, fuzzy, c-format
msgid "Level: %d\tScore: %d"
msgstr "స్కోర్: %d"

#. Second line of safe teleports button label. %d is the number of teleports remaining.
#: ../src/gnome-robots.c:167
#, fuzzy, c-format
msgid "(Remaining: %d)"
msgstr "మిగిలివున్న:"

#. First line of safe teleports button label.
#: ../src/gnome-robots.c:169
#, fuzzy
msgid "Teleport _Safely"
msgstr "టెలీపోర్టు యాదృశ్చికంగా"

#: ../src/gnome-robots.c:227
#, fuzzy
msgid "Based on classic BSD Robots"
msgstr "సాంప్రదాయ రోబోట్సు"

#: ../src/gnome-robots.c:231
msgid "translator-credits"
msgstr "veera reddy v <veerared.e@gmail.com>"

#: ../src/gnome-robots.c:252
#, fuzzy
msgid "Are you sure you want to discard the current game?"
msgstr "మీరు నిజంగా త్యజించదల్చుకున్నారా?"

#: ../src/gnome-robots.c:255
#, fuzzy
msgid "Keep _Playing"
msgstr "ఆడుతున్నది"

#: ../src/gnome-robots.c:387
#, fuzzy
msgid "Teleport _Randomly"
msgstr "టెలీపోర్టు యాదృశ్చికంగా"

#: ../src/gnome-robots.c:406
#, fuzzy
msgid "_Wait for Robots"
msgstr "రోబోట్సు కొరకు వేచివుండండి"

#. Label on the scores dialog, next to map type dropdown
#: ../src/gnome-robots.c:430
msgid "Game Type:"
msgstr "ఆట రకం:"

#: ../src/gnome-robots.c:445
msgid "No game data could be found."
msgstr "ఏ ఆట దత్తాంశం కనబడలేదు."

#: ../src/gnome-robots.c:447
msgid ""
"The program Robots was unable to find any valid game configuration files. "
"Please check that the program is installed correctly."
msgstr ""
"ప్రోగ్రామ్ రోబోట్సు ఏ విలువైన ఆట ఆకృతీకరణ దస్త్రములను కనుగొనలేక పోయింది. ప్రోగ్రామ్ సరిగా "
"సంస్థాపించబడిందేమో దయచేసి పరిశీలించండి."

#: ../src/gnome-robots.c:464
msgid "Some graphics files are missing or corrupt."
msgstr "కొన్ని చిత్రరూప దస్త్రాలు కనుపడుటలేదు లేదా పాడయినవి"

#: ../src/gnome-robots.c:466
msgid ""
"The program Robots was unable to load all the necessary graphics files. "
"Please check that the program is installed correctly."
msgstr ""
"ప్రోగ్రామ్ రోబోట్సు కావలసిన గ్రాఫిక్ దస్త్రములను లోడు చేయలేక పోయింది. దయచేసి ప్రోగ్రామ్ సరిగా "
"సంస్థాపించబడునట్లు పరిశీలించండి."

#: ../src/graphics.c:149
#, c-format
msgid "Could not find '%s' pixmap file\n"
msgstr "'%s' పిక్సుమాప్ దస్త్రమును కనుగొనలేక పోయింది\n"

#: ../src/properties.c:391
msgid "Preferences"
msgstr "అభీష్టాలు"

#: ../src/properties.c:415
msgid "Game Type"
msgstr "ఆట రకం"

#: ../src/properties.c:425
msgid "_Use safe moves"
msgstr "భద్రమైన ఎత్తులను ఉపయోగించుము (_U)"

#: ../src/properties.c:430
msgid "Prevent accidental moves that result in getting killed."
msgstr "చనిపోవుటకు కారణమగు ప్రమాదకర కదలికలను నిరోదిస్తుంది."

#: ../src/properties.c:433
msgid "U_se super safe moves"
msgstr "సూపర్ సేఫ్ కదలికలను వుపయోగించుము (_s)"

#: ../src/properties.c:440
msgid "Prevents all moves that result in getting killed."
msgstr "చనిపోవుటకు కారణమగు అన్ని కదలికలను నిరోధిస్తుంది."

#: ../src/properties.c:446
msgid "_Enable sounds"
msgstr "ధ్వనులను క్రియాశీలీకరించు (_E)"

#: ../src/properties.c:452
msgid "Play sounds for events like winning a level and dying."
msgstr "ఒక స్థాయి గెలుచుట మరియు చనిపోవుట వంటి ఘటనల కొరకు శబ్దములు."

#: ../src/properties.c:454
msgid "Game"
msgstr "ఆట"

#: ../src/properties.c:467
msgid "_Image theme:"
msgstr "ప్రతిరూపం వైవిధ్యాంశం (_I):"

#: ../src/properties.c:479
msgid "_Background color:"
msgstr "పూర్వరంగం వర్ణము (_B):"

#: ../src/properties.c:491
msgid "Appearance"
msgstr "రూపం"

#: ../src/properties.c:520
msgid "_Restore Defaults"
msgstr "అప్రమేయాలను మళ్ళీదాయుము (_R)"

#: ../src/properties.c:525
msgid "Keyboard"
msgstr "మీట పలకం"
